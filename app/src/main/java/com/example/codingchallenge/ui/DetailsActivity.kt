package com.example.codingchallenge.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.navArgs
import coil.load
import com.example.codingchallenge.R
import com.example.codingchallenge.models.Result
import kotlinx.android.synthetic.main.activity_details.*
import android.preference.PreferenceManager

import android.content.SharedPreferences




class DetailsActivity : AppCompatActivity() {

    private val args by navArgs<DetailsActivityArgs>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val args = args
        val myBundle: Result = args?.result

        imageViewDetails.load(myBundle?.artworkUrl100)
        textViewLongDescription.text = myBundle?.longDescription

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}