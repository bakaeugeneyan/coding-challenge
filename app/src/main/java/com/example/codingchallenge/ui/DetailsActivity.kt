package com.example.codingchallenge.ui

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.navigation.navArgs
import coil.load
import com.example.codingchallenge.R
import com.example.codingchallenge.data.database.entities.FavoriteEntity
import com.example.codingchallenge.models.Result
import com.example.codingchallenge.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_details.*
import java.lang.Exception

@AndroidEntryPoint
class DetailsActivity : AppCompatActivity() {

    private val args by navArgs<DetailsActivityArgs>()
    private val mainViewModel: MainViewModel by viewModels()

    private var favoriteSave = false
    private var saveFavoriteMovieId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Details"

        val args = args
        val myBundle: Result = args?.result

        imageViewDetails.load(myBundle?.artworkUrl100)
        textViewLongDescription.text = myBundle?.longDescription

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.details_menu, menu)
        val menuItem = menu?.findItem(R.id.save_to_favorites_menu)
        checkSaveFavorite(menuItem!!)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.save_to_favorites_menu && !favoriteSave) {
            saveToFavorite(item)
        } else if (item.itemId == R.id.save_to_favorites_menu && favoriteSave) {
            removeFromFavorite(item)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun checkSaveFavorite(menuItem: MenuItem) {
        mainViewModel.readFavoriteMovie.observe(this, { favoriteEntity ->
            try {
                for (saveFavorite in favoriteEntity) {
                    if (saveFavorite.result.trackId == args.result.trackId){
                        changeMenuItemColor(menuItem, R.color.yellow)
                        saveFavoriteMovieId = saveFavorite.id
                        favoriteSave = true
                    } else {
                        changeMenuItemColor(menuItem, R.color.white)
                    }
                }
            }catch (e: Exception) {
                Log.d("DetailsActivity", e.message.toString())
            }
        })
    }

    private fun saveToFavorite(item: MenuItem) {
        val favoriteEntity = FavoriteEntity(
            0,
            args.result
        )
        mainViewModel.insertFavoriteMovie(favoriteEntity)
        changeFavoriteIconColor(item, R.color.yellow)
        showSnackBar("Favorite movie saved.")
        favoriteSave = false
    }

    private fun removeFromFavorite(item: MenuItem){
        val favoriteEntity = FavoriteEntity(saveFavoriteMovieId, args.result)
        mainViewModel.deleteFavoriteMovie(favoriteEntity)
        changeMenuItemColor(item, R.color.white)
        showSnackBar("Remove from Favorite")
        favoriteSave = false
    }

    private fun showSnackBar(message: String) {
        Snackbar.make(detailsActivityLayout, message, Snackbar.LENGTH_SHORT).setAction("Okay"){}.show()
    }

    private fun changeFavoriteIconColor(item: MenuItem, color: Int) {
        item.icon.setTint(ContextCompat.getColor(this, color))
    }

    private fun changeMenuItemColor(item: MenuItem, color: Int) {
        item.icon.setTint(ContextCompat.getColor(this, color))
    }
}