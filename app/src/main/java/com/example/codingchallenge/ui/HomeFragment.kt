package com.example.codingchallenge.ui

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.codingchallenge.R
import com.example.codingchallenge.adapters.ItunesAdapter
import com.example.codingchallenge.databinding.FragmentHomeBinding
import com.example.codingchallenge.utils.NetworkListener
import com.example.codingchallenge.utils.NetworkResult
import com.example.codingchallenge.utils.observeOnce
import com.example.codingchallenge.viewmodel.ItunesViewModel
import com.example.codingchallenge.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class HomeFragment : Fragment(), SearchView.OnQueryTextListener
{
    private  var _binding: FragmentHomeBinding? = null
    private val binding get () = _binding!!
    private lateinit var mainViewModel: MainViewModel
    private lateinit var itunesViewModel: ItunesViewModel
    private val mAdapter by lazy { ItunesAdapter() }

    private lateinit var networkListener: NetworkListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainViewModel  = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)
        itunesViewModel = ViewModelProvider(requireActivity()).get(ItunesViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding =  FragmentHomeBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.mainViewModel = mainViewModel
        setHasOptionsMenu(true)

        setupRecycleView()

        itunesViewModel.readBackOnline.observe(viewLifecycleOwner, {
            itunesViewModel.backOnline = it
        })

        lifecycleScope.launch {
            networkListener = NetworkListener()
            networkListener.checkNetworkAvailability(requireContext())
                .collect { status ->
                    Log.d("NetworkListener", status.toString())
                    itunesViewModel.netWorkStatus = status
                    itunesViewModel.showNetworkStatus()
                    readDatabase()
                }
        }

        return binding.root
    }

    private fun setupRecycleView(){
        binding.recyclerView.adapter = mAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        showShimmerEffect()
    }

    private fun readDatabase() {
        showShimmerEffect()
        lifecycleScope.launch {
            mainViewModel.readItunesList.observeOnce(viewLifecycleOwner, { database ->
                if (database.isNotEmpty()) {
                    Log.d("homeFragment", "readDatabase called!")
                    mAdapter.setData(database[0].itunesList)
                    hideShimmerEffect()
                }else {
                    requestApiData()
                }
            })
        }
    }

    private fun requestApiData() {
        Log.d("homeFragment", "requestApiData called!")
        mainViewModel.getItunesList(itunesViewModel.applyQueries())
        mainViewModel.itunesResponse.observe(viewLifecycleOwner, { response ->
            when(response) {
                is NetworkResult.Success -> {
                    hideShimmerEffect()
                    response.data?.let { mAdapter.setData(it) }
                }
                is NetworkResult.Error -> {
                    hideShimmerEffect()
                    loadDataFromCache()
                    Toast.makeText(requireContext(), response.message.toString(), Toast.LENGTH_SHORT).show()
                }
                is NetworkResult.Loading -> {
                    showShimmerEffect()
                }
            }
        })
    }

    private fun searchApiData(searchQuery: String) {
        showShimmerEffect()
        mainViewModel.searchItunes(itunesViewModel.applySearchQuery(searchQuery))
        mainViewModel.searchedItunesResponse.observe(this, { response ->
            when(response) {
                is NetworkResult.Success -> {
                    hideShimmerEffect()
                    val itunes = response.data
                    itunes?.let { mAdapter.setData(it) }
                }
                is NetworkResult.Error -> {
                    hideShimmerEffect()
                    loadDataFromCache()
                    no_data_found_tv.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
//                    Toast.makeText(requireContext(), response.message.toString(), Toast.LENGTH_SHORT).show()
                }
                is NetworkResult.Loading -> {
                    showShimmerEffect()
                }
            }
        })
    }

    private fun loadDataFromCache() {
        Log.d("homeFragment", "loadDataFromCache called!")
        lifecycleScope.launch {
            mainViewModel.readItunesList.observe(viewLifecycleOwner, { database ->
                if (database.isNotEmpty()) {
                    mAdapter.setData(database[0].itunesList)
                }
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)

        val search = menu.findItem(R.id.menu_search)
        val searchView = search.actionView as? SearchView
        searchView?.isSubmitButtonEnabled = true
        searchView?.setOnQueryTextListener(this)
    }

    private fun showShimmerEffect() {
        binding.recyclerView.showShimmer()
    }

    private fun hideShimmerEffect() {
        binding.recyclerView.hideShimmer()
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (query != null) {
            searchApiData(query)
        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}