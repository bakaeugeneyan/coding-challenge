package com.example.codingchallenge.models

import com.google.gson.annotations.SerializedName

data class ItunesList(
    @SerializedName("resultCount")
    val resultCount: Int,
    @SerializedName("results")
    val results: List<Result>
)