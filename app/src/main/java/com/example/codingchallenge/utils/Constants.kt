package com.example.codingchallenge.utils

// in this class i add companion object because one advantage that companion objects have over static
// members is that they can inherit from other classes or implement interfaces and generally behave.
class Constants {
    companion object {
        const val BASE_URL = "https://itunes.apple.com"

        // API Query Keys
        const val QUERY_SEARCH = "query"
        const val QUERY_NUMBER = "number"
        const val DEFAULT_ITUNES_NUMBER = "50"
        const val QUERY_COUNTRY = "au"
        const val QUERY_MEDIA = "movie"

        // Room Database
        const val DATABASE_NAME = "dyip_pay_db"
        const val ITUNES_TABLE = "itunes_table"

    }
}