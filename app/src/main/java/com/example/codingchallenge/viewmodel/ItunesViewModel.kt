package com.example.codingchallenge.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.codingchallenge.utils.Constants.Companion.DEFAULT_ITUNES_NUMBER
import com.example.codingchallenge.utils.Constants.Companion.QUERY_COUNTRY
import com.example.codingchallenge.utils.Constants.Companion.QUERY_MEDIA
import com.example.codingchallenge.utils.Constants.Companion.QUERY_NUMBER
import com.example.codingchallenge.utils.Constants.Companion.QUERY_SEARCH

class ItunesViewModel(application: Application): AndroidViewModel(application) {

    fun applyQueries(): HashMap<String, String> {
        val queries: HashMap<String, String> = HashMap()
        queries["numbers"] = DEFAULT_ITUNES_NUMBER
        queries["amp;country"] = QUERY_COUNTRY
        queries["amp;media"] = QUERY_MEDIA
        queries["amp;all"] = ""
        return queries
    }

    fun applySearchQuery(searchQuery: String): HashMap<String, String> {
        val queries: HashMap<String, String> = HashMap()
        queries["term"] = searchQuery
        queries["numbers"] = DEFAULT_ITUNES_NUMBER
        queries["amp;country"] = QUERY_COUNTRY
        queries["amp;media"] = QUERY_MEDIA
        queries["amp;all"] = ""
        return queries
    }
}