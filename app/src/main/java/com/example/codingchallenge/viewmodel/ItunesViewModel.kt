package com.example.codingchallenge.viewmodel

import android.app.Application
import android.widget.Toast
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.codingchallenge.data.DataStoreRepository
import com.example.codingchallenge.utils.Constants.Companion.DEFAULT_ITUNES_NUMBER
import com.example.codingchallenge.utils.Constants.Companion.QUERY_COUNTRY
import com.example.codingchallenge.utils.Constants.Companion.QUERY_MEDIA
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ItunesViewModel @ViewModelInject constructor(
    application: Application,
    private val dataStoreRepository: DataStoreRepository
) : AndroidViewModel(application) {

    var netWorkStatus = false
    var backOnline = false

    val readBackOnline = dataStoreRepository.readBackOnline.asLiveData()

    fun saveBackOnline(backOnline: Boolean) =
        viewModelScope.launch(Dispatchers.IO){
            dataStoreRepository.saveBackOnline(backOnline)
        }

    fun applyQueries(): HashMap<String, String> {
        val queries: HashMap<String, String> = HashMap()
        queries["numbers"] = DEFAULT_ITUNES_NUMBER
        queries["amp;country"] = QUERY_COUNTRY
        queries["amp;media"] = QUERY_MEDIA
        queries["amp;all"] = ""
        return queries
    }

    fun applySearchQuery(searchQuery: String): HashMap<String, String> {
        val queries: HashMap<String, String> = HashMap()
        queries["term"] = searchQuery
        queries["numbers"] = DEFAULT_ITUNES_NUMBER
        queries["amp;country"] = QUERY_COUNTRY
        queries["amp;media"] = QUERY_MEDIA
        queries["amp;all"] = ""
        return queries
    }

    fun showNetworkStatus(){
        if (!netWorkStatus) {
            Toast.makeText(getApplication(), "No Internet Connection.", Toast.LENGTH_SHORT).show()
            saveBackOnline(true)
        }else if(netWorkStatus){
            if(backOnline){
                Toast.makeText(getApplication(), "We're back online.", Toast.LENGTH_SHORT).show()
                saveBackOnline(false)
            }
        }
    }

}