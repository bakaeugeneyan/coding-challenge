package com.example.codingchallenge.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.lifecycle.*
import com.example.codingchallenge.data.Repository
import com.example.codingchallenge.data.database.ItunesEntity
import com.example.codingchallenge.models.ItunesList
import com.example.codingchallenge.utils.NetworkResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: Repository,
    application: Application
): AndroidViewModel(application) {

    /** Room Database **/
    val readItunesList: LiveData<List<ItunesEntity>> = repository.local.readDatabase().asLiveData()

    private fun insertItunes(itunesEntity: ItunesEntity) =
        viewModelScope.launch(Dispatchers.IO) {
            repository.local.insertItunes(itunesEntity)
        }

    /** Retrofit **/
    var itunesResponse: MutableLiveData<NetworkResult<ItunesList>> = MutableLiveData()
    var searchedItunesResponse : MutableLiveData<NetworkResult<ItunesList>> = MutableLiveData()

    fun getItunesList(queries: Map<String, String>) = viewModelScope.launch {
        getItunesSafeCall(queries)
    }

    fun searchItunes(searchQuery: Map<String, String>) = viewModelScope.launch {
        searchSafeCall(searchQuery)
    }

    private suspend fun getItunesSafeCall(queries: Map<String, String>) {
        itunesResponse.value = NetworkResult.Loading()

        if (hasInternetConnection()) {
            try {
                val response = repository.remote.getItunesList(queries)
                itunesResponse.value = handleItunesResponse(response)

                val itunes = itunesResponse.value!!.data
                if (itunes != null) {
                    offlineCacheItunes(itunes)
                }
            }
            catch (e: Exception) {
                itunesResponse.value = NetworkResult.Error("Itunes Not Found")
            }
        } else {
            itunesResponse.value = NetworkResult.Error("No Internet Connection.")
        }
    }

    private fun offlineCacheItunes(itunesList: ItunesList) {
        val itunesEntity = ItunesEntity(itunesList)
        insertItunes(itunesEntity)
    }

    private suspend fun searchSafeCall(searchQuery: Map<String, String>) {
        searchedItunesResponse.value = NetworkResult.Loading()

        if (hasInternetConnection()) {
            try {
                val response = repository.remote.searchItunesList(searchQuery)
                searchedItunesResponse.value = handleItunesResponse(response)
            }
            catch (e: Exception) {
                searchedItunesResponse.value = NetworkResult.Error("Itunes Not Found")
            }
        } else {
            searchedItunesResponse.value = NetworkResult.Error("No Internet Connection.")
        }
    }

    private fun handleItunesResponse(response: Response<ItunesList>): NetworkResult<ItunesList>? {
        when {
            response.message().toString().contains("timeout") -> {
                return NetworkResult.Error("Timeout")
            }
            response.body()!!.results.isNullOrEmpty() -> {
                return NetworkResult.Error("Itunes not found.")
            }
            response.isSuccessful -> {
                val itunes = response.body()
                return NetworkResult.Success(itunes!!)
            }
            else -> {
                return NetworkResult.Error(response.message())
            }
        }
    }

    private fun hasInternetConnection(): Boolean {
        val connectivityManager = getApplication<Application>().getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetwork ?: return false
        val capabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
        return when {
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }
}