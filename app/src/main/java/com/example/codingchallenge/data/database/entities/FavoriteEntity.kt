package com.example.codingchallenge.data.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.codingchallenge.models.Result
import com.example.codingchallenge.utils.Constants.Companion.FAVORITE_TABLE

@Entity(tableName = FAVORITE_TABLE)
class FavoriteEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var result: Result
)