package com.example.codingchallenge.data.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.codingchallenge.models.ItunesList
import com.example.codingchallenge.utils.Constants.Companion.ITUNES_TABLE

@Entity(tableName = ITUNES_TABLE)
class ItunesEntity(
    var itunesList: ItunesList
) {
    @PrimaryKey(autoGenerate = false)
    var id: Int = 0
}