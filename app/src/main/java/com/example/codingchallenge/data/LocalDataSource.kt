package com.example.codingchallenge.data

import com.example.codingchallenge.data.database.ItunesDao
import com.example.codingchallenge.data.database.ItunesEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LocalDataSource @Inject constructor(
    private val itunesDao: ItunesDao
) {

    fun readDatabase(): Flow<List<ItunesEntity>> {
        return itunesDao.readItunes()
    }

    suspend fun insertItunes(itunesEntity: ItunesEntity) {
        itunesDao.insertItunes(itunesEntity)
    }
}