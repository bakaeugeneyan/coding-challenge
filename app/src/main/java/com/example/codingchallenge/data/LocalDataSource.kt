package com.example.codingchallenge.data

import com.example.codingchallenge.data.database.ItunesDao
import com.example.codingchallenge.data.database.entities.FavoriteEntity
import com.example.codingchallenge.data.database.entities.ItunesEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LocalDataSource @Inject constructor(
    private val itunesDao: ItunesDao
) {

    fun readDatabase(): Flow<List<ItunesEntity>> {
        return itunesDao.readItunes()
    }

    suspend fun insertItunes(itunesEntity: ItunesEntity) {
        itunesDao.insertItunes(itunesEntity)
    }

    fun readFavoriteMovie(): Flow<List<FavoriteEntity>>{
        return itunesDao.readFavoriteMovie()
    }

    suspend fun insertFavoriteMovie(favoriteEntity: FavoriteEntity){
        itunesDao.insertFavoriteMovie(favoriteEntity)
    }

    suspend fun deleteFavoriteMovie(favoriteEntity: FavoriteEntity){
        itunesDao.deleteFavoriteMovie(favoriteEntity)
    }

    suspend fun deleteAllFavoriteMovie(){
        itunesDao.deleteAllFavoriteMovie()
    }

}