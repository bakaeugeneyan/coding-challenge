package com.example.codingchallenge.data.database

import androidx.room.TypeConverter
import com.example.codingchallenge.models.ItunesList
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ItunesTypeConverter {

    var gson = Gson()

    @TypeConverter
    fun itunesToString(itunesList: ItunesList): String {
        return gson.toJson(itunesList)
    }

    @TypeConverter
    fun stringToItunes(data: String) : ItunesList {
        val listType = object : TypeToken<ItunesList>() {}.type
        return gson.fromJson(data, listType)
    }
}