package com.example.codingchallenge.data.database

import androidx.room.TypeConverter
import com.example.codingchallenge.models.ItunesList
import com.example.codingchallenge.models.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class ItunesTypeConverter {

    var gson = Gson()

    @TypeConverter
    fun itunesToString(itunesList: ItunesList): String {
        return gson.toJson(itunesList)
    }

    @TypeConverter
    fun stringToItunes(data: String) : ItunesList {
        val listType = object : TypeToken<ItunesList>() {}.type
        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun resultToString(result: Result): String {
        return gson.toJson(result)
    }

    @TypeConverter
    fun stringToResult(data: String): Result {
        val listType = object : TypeToken<Result>() {}.type
        return gson.fromJson(data, listType)
    }
}