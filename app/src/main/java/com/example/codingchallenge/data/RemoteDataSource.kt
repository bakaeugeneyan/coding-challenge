package com.example.codingchallenge.data

import com.example.codingchallenge.Interface.ItunesApi
import com.example.codingchallenge.models.ItunesList
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val itunesApi: ItunesApi
) {

    suspend fun getItunesList(queries: Map<String, String>): Response<ItunesList> {
        return itunesApi.getItunesList(queries)
    }

    suspend fun searchItunesList(searchQuery: Map<String, String>): Response<ItunesList>{
        return  itunesApi.searchItunes(searchQuery)
    }
}