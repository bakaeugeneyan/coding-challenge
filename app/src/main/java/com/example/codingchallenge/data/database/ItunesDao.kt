package com.example.codingchallenge.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface ItunesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertItunes(itunesEntity: ItunesEntity)

    @Query("SELECT * FROM itunes_table ORDER BY id ASC")
    fun readItunes(): Flow<List<ItunesEntity>>
}