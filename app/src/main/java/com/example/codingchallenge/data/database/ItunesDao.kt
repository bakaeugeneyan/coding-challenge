package com.example.codingchallenge.data.database

import androidx.room.*
import com.example.codingchallenge.data.database.entities.FavoriteEntity
import com.example.codingchallenge.data.database.entities.ItunesEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ItunesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertItunes(itunesEntity: ItunesEntity)

    @Query("SELECT * FROM itunes_table ORDER BY id ASC")
    fun readItunes(): Flow<List<ItunesEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFavoriteMovie(favoritesEntity: FavoriteEntity)

    @Query("SELECT * FROM favorite_table ORDER BY id ASC")
    fun readFavoriteMovie(): Flow<List<FavoriteEntity>>

    @Delete
    suspend fun deleteFavoriteMovie(favoriteEntity: FavoriteEntity)

    @Query("DELETE FROM favorite_table")
    suspend fun deleteAllFavoriteMovie()
}