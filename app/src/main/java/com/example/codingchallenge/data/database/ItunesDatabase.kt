package com.example.codingchallenge.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(
    entities = [ItunesEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(ItunesTypeConverter::class)
abstract class ItunesDatabase : RoomDatabase(){

    abstract fun itunesDao(): ItunesDao
}