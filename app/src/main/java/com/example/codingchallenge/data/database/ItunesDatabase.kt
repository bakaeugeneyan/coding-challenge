package com.example.codingchallenge.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.codingchallenge.data.database.entities.FavoriteEntity
import com.example.codingchallenge.data.database.entities.ItunesEntity

@Database(
    entities = [ItunesEntity::class, FavoriteEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(ItunesTypeConverter::class)
abstract class ItunesDatabase : RoomDatabase(){

    abstract fun itunesDao(): ItunesDao
}