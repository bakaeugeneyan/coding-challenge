package com.example.codingchallenge.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.codingchallenge.utils.ItunesDiffUtil
import com.example.codingchallenge.databinding.ItunesListPlaceholderRowLayoutBinding
import com.example.codingchallenge.models.ItunesList
import com.example.codingchallenge.models.Result

class ItunesAdapter : RecyclerView.Adapter<ItunesAdapter.MyViewHolder>() {

    private var itunes = emptyList<Result>()

    class MyViewHolder(private val binding: ItunesListPlaceholderRowLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(result: Result){
            binding.result = result
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): MyViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItunesListPlaceholderRowLayoutBinding.inflate(layoutInflater, parent, false)
                return  MyViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return  MyViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       val currentItunes = itunes[position]
        holder.bind(currentItunes)
    }

    override fun getItemCount(): Int {
        return itunes.size
    }

    fun setData(newData: ItunesList) {
        val itunesDiffUtil = ItunesDiffUtil(itunes, newData.results)
        val diffUtilResult = DiffUtil.calculateDiff(itunesDiffUtil)
        itunes = newData.results
        diffUtilResult.dispatchUpdatesTo(this)
    }
}