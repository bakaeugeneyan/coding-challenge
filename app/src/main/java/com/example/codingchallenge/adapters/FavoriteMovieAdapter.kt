package com.example.codingchallenge.adapters

import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.codingchallenge.R
import com.example.codingchallenge.data.database.entities.FavoriteEntity
import com.example.codingchallenge.databinding.FavoriteListRowLayoutBinding
import com.example.codingchallenge.ui.FavoriteFragmentDirections
import com.example.codingchallenge.utils.ItunesDiffUtil
import com.example.codingchallenge.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.favorite_list_row_layout.view.*

class FavoriteMovieAdapter(
    private val requireActivity: FragmentActivity,
    private val mainViewModel: MainViewModel
): RecyclerView.Adapter<FavoriteMovieAdapter.MyViewHolder>(), ActionMode.Callback {

    private var multiSelection = false

    private lateinit var mActionMode: ActionMode
    private lateinit var rootView: View

    private var favoriteMovie = emptyList<FavoriteEntity>()
    private var myViewHolder = arrayListOf<MyViewHolder>()
    private var selectedMovie = arrayListOf<FavoriteEntity>()

    class MyViewHolder(private val binding: FavoriteListRowLayoutBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(favoritesEntity: FavoriteEntity){
            binding.favoriteEntity = favoritesEntity
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): MyViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = FavoriteListRowLayoutBinding.inflate(layoutInflater, parent, false)
                return MyViewHolder(binding)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        myViewHolder.add(holder)
        rootView = holder.itemView.rootView

        val currentFavoriteMovie = favoriteMovie[position]
        holder.bind(currentFavoriteMovie)

//        Single Click Listener
        holder.itemView.favoriteRowLayout.setOnClickListener {
            if(multiSelection){
                applySelection(holder,currentFavoriteMovie)
            } else {
                val action = FavoriteFragmentDirections.actionFavoriteFragmentToDetailsActivity(currentFavoriteMovie.result)
                holder.itemView.findNavController().navigate(action)
            }
        }

        //        Long Click Listener
        holder.itemView.favoriteRowLayout.setOnLongClickListener {
            if (!multiSelection){
                multiSelection = true
                requireActivity.startActionMode(this)
                applySelection(holder, currentFavoriteMovie)
                true
            }else {
                multiSelection = false
                false
            }
        }
    }

    private fun applySelection(holder: MyViewHolder, currentRecipe: FavoriteEntity){
        if (selectedMovie.contains(currentRecipe)){
            selectedMovie.remove(currentRecipe)
            changeFavoriteMovieStyle(holder, R.color.cardBackgroundColor, R.color.strokeColor)
            applyActionModeTitle()
        }else {
            selectedMovie.add(currentRecipe)
            changeFavoriteMovieStyle(holder, R.color.cardBackgroundLightColor, R.color.colorPrimary)
            applyActionModeTitle()
        }
    }

    private fun changeFavoriteMovieStyle(holder: MyViewHolder, backgroundColor: Int, strokeColor: Int){
        holder.itemView.favoriteRowLayout.setBackgroundColor(
            ContextCompat.getColor(requireActivity, backgroundColor)
        )
        holder.itemView.favorite_list_row_mc.strokeColor = ContextCompat.getColor(requireActivity, strokeColor)
    }

    private fun applyActionModeTitle(){
        when(selectedMovie.size) {
             0 -> {
                 mActionMode.finish()
             }
            1 -> {
                mActionMode.title = "${selectedMovie.size} item Selected"
            }
            else -> {
                mActionMode.title = "${selectedMovie.size} items Selected"
            }
        }
    }

    override fun getItemCount(): Int {
        return favoriteMovie.size
    }

    override fun onCreateActionMode(actionMode: ActionMode?, menu: Menu?): Boolean {
        actionMode?.menuInflater?.inflate(R.menu.favorite_movie_delete_menu, menu)
        mActionMode = actionMode!!
        applyStatusBarColor(R.color.contextualStatusBarColor)
        return true
    }

    override fun onPrepareActionMode(actionMode: ActionMode?, menu: Menu?): Boolean {
        return true
    }

    override fun onActionItemClicked(actionMode: ActionMode?, menu: MenuItem?): Boolean {
        if(menu?.itemId == R.id.delete_favorite_movie_menu){
            selectedMovie.forEach {
                mainViewModel.deleteFavoriteMovie(it)
            }
            showSnackBar("${selectedMovie.size} Favorite movie/s removed.")
            multiSelection = false
            selectedMovie.clear()
            actionMode?.finish()
        }
        return true
    }

    override fun onDestroyActionMode(actionMode: ActionMode?) {
        myViewHolder.forEach { holder ->
            changeFavoriteMovieStyle(holder, R.color.cardBackgroundColor, R.color.strokeColor)
        }

        multiSelection = false
        selectedMovie.clear()
        applyStatusBarColor(R.color.statusBarColor)
    }

    private fun applyStatusBarColor(color: Int){
        requireActivity.window.statusBarColor = ContextCompat.getColor(requireActivity, color)
    }

    fun setData(newFavoriteMovie: List<FavoriteEntity>) {
        val favoriteMovieDiffUtil = ItunesDiffUtil(favoriteMovie, newFavoriteMovie)
        val diffUtilResult = DiffUtil.calculateDiff(favoriteMovieDiffUtil)
        favoriteMovie = newFavoriteMovie
        diffUtilResult.dispatchUpdatesTo(this)
    }

    private fun showSnackBar(message: String) {
        Snackbar.make(rootView,  message, Snackbar.LENGTH_SHORT).setAction("Okay"){}.show()
    }

    fun clearContextualActionMode(){
        if(this::mActionMode.isInitialized){
            mActionMode.finish()
        }
    }
}