package com.example.codingchallenge.bindingadapters

import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.navigation.findNavController
import androidx.navigation.navArgs
import coil.load
import com.example.codingchallenge.R
import com.example.codingchallenge.models.Result
import com.example.codingchallenge.ui.DetailsActivityArgs
import com.example.codingchallenge.ui.HomeFragmentDirections

class ItunesRowBinding {

    companion object {
        @BindingAdapter("onItunesHolderClickListener")
        @JvmStatic
        fun onItunesHolderClickListener(itunesRowLayout: ConstraintLayout, result: Result) {
            Log.d("onItunesHolderClickListener", "Called!")
            itunesRowLayout.setOnClickListener {
                try {
                    val action = HomeFragmentDirections.actionHomeFragmentToDetailsActivity(result)
                    itunesRowLayout.findNavController().navigate(action)
                }catch (e: Exception) {
                    Log.d("onItunesHolderClickListener", e.toString())
                }
            }
        }

        @BindingAdapter("loadImageFromUrl")
        @JvmStatic
        fun loadImageFromUrl(imageView: ImageView, imageUrl: String){
            imageView.load(imageUrl) {
                crossfade(600)
                error(R.drawable.image_error_placeholder)
            }
        }

        @BindingAdapter("setTrackPrice")
        @JvmStatic
        fun setTrackPrice(textView: TextView, price: Double){
            textView.text = price.toString()
        }
    }
}