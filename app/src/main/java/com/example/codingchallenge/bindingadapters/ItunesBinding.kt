package com.example.codingchallenge.bindingadapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.codingchallenge.data.database.entities.ItunesEntity
import com.example.codingchallenge.models.ItunesList
import com.example.codingchallenge.utils.NetworkResult

class ItunesBinding {

    companion object {

        @BindingAdapter("readApiResponse", "readDatabase", requireAll = true)
        @JvmStatic
        fun errorImageViewVisibility(
            imageView: ImageView,
            apiResponse: NetworkResult<ItunesList>?,
            database: List<ItunesEntity>?
        ){
            if (apiResponse is NetworkResult.Error && database.isNullOrEmpty()) {
                imageView.visibility = View.VISIBLE
            }else if (apiResponse is NetworkResult.Loading) {
                imageView.visibility = View.INVISIBLE
            }else if (apiResponse is NetworkResult.Success) {
                imageView.visibility = View.INVISIBLE
            }
        }

        @BindingAdapter("readApiResponseTextView", "readDatabaseTextView", requireAll = true)
        @JvmStatic
        fun errorTextViewVisibility(
            textView: TextView,
            apiResponse: NetworkResult<ItunesList>?,
            database: List<ItunesEntity>?
        ){
            if (apiResponse is NetworkResult.Error && database.isNullOrEmpty()) {
                textView.visibility = View.VISIBLE
                textView.text = apiResponse.message.toString()
            }else if (apiResponse is NetworkResult.Loading) {
                textView.visibility = View.INVISIBLE
            }else if (apiResponse is NetworkResult.Success) {
                textView.visibility = View.INVISIBLE
            }
        }
    }
}