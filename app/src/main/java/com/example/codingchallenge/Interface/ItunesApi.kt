package com.example.codingchallenge.Interface

import com.example.codingchallenge.models.ItunesList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ItunesApi {

    @GET("/search?term=star&amp;country=au&amp;media=movie&amp;all")
    suspend fun getItunesList(
        @QueryMap queries: Map<String, String>
    ): Response<ItunesList>

    @GET("/search?term=star&amp;country=au&amp;media=movie&amp;all")
    suspend fun searchItunes(
        @QueryMap searchQuery: Map<String, String>
    ): Response<ItunesList>
}